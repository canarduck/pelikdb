Pelikdb est un thème pour [pelican](http://getpelican.com). Il n'est pas pensé pour un blog, mais plus pour une base de connaissances (d'où le KDB).

Il utilise le framework [Bootstrap](http://getbootstrap.com/) et s'inspire très largement de la mise en page de sa documentation.

## Réglages

Dans pelicanconf.py

    ::python
    DEFAULT_PAGINATION = False

    # URLs
    ARTICLE_URL = '{slug}.html'

    # Feeds
    FEED_ALL_ATOM = None
    CATEGORY_FEED_ATOM = None
    TRANSLATION_FEED_ATOM = None
    AUTHOR_FEED_ATOM = None
    AUTHOR_FEED_RSS = None

    # Theme
    THEME = 'pelikdb'


Facultatif :

    ::sh

    # Isso 
    ISSO_URL = '//comment.example.tld' 

    # Piwik
    PIWIK_URL = '//stats.example.tld'
    PIWIK_SITE_ID = 9999

    # License pour le contenu
    LICENCE_URL = '' # par exemple http://creativecommons.org/licenses/by-sa/4.0/


## Compilation

Pour modifier la css il faut installer un compilateur less (lessc) et Bootstrap via Bower. Bower & lessc sont des modules nodejs,  

    ::sh
    bower install bootstrap

Sans bower on peut également [télécharger les sources de Bootstrap](http://getbootstrap.com/getting-started/#download) et les placer dans un répertoire ```bower_components/bootstrap``` à la racine de pelikdb.

Une fois pelikdb.less ou theme.less modifié, le CSS est à recompiler avec

    ::sh
    lessc -x pelikdb.less static/css/main.css

## Pygment

Il est possible de modifier le style utilisé par pygment, un aperçu des différents thèmes est disponible sur http://help.farbox.com/pygments.html

    ::sh
    pygmentize -S NOMDUSTYLE -f html -a .highlight > pygment.css
    cp pygment.css 
